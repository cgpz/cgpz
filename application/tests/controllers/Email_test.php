<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require 'C:\wamp64\cgpz\application\vendor\autoload.php';
include 'C:\wamp64\cgpz\application\models\email.php';

/**
 * Description of Email_test
 *
 * @author dmoll
 */
class Email_test extends TestCase {
    
    private $emailController;
    
    public function setUp() 
    {
        $this->emailController = new Email();
    }
    
    public function test_SendEmailThroughController()
    {                
        $emailAddress = 'dmoll@umich.edu';
        
        $verificationCode = $this->emailController->generateEmailVerificationCode();
        
        $emailSent = $this->emailController->sendVerificationEmail($emailAddress, $verificationCode);
        
        $this->assertTrue($emailSent);
    }
    
    public function SendEmailNoCI()
    {
        // Instantiate a new PHPMailer 
        $mail = new PHPMailer;

        // Tell PHPMailer to use SMTP
        $mail->isSMTP();
        $mail->SMTPDebug = 2;

        // If you're using Amazon SES in a Region other than US West (Oregon), 
        // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
        // endpoint in the appropriate Region.
        $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        // Tells PHPMailer to use SMTP authentication
        $mail->SMTPAuth = true;

        // Replace smtp_username with your Amazon SES SMTP user name.
        $mail->Username = 'AKIAIP2LFU57E7PBO5IA';

        // Replace smtp_password with your Amazon SES SMTP password.
        $mail->Password = 'Ai44sRPn3yzNdWVNNEFoQavMVfLufno8Af/HvAOaf0Fd';

        // Enable SSL encryption
        $mail->SMTPSecure = 'ssl';

        // The port you will connect to on the Amazon SES SMTP endpoint.
        $mail->Port = 465;

        // Replace sender@example.com with your "From" address. 
        // This address must be verified with Amazon SES.
        $mail->setFrom('dmoll@emich.edu', 'David Moll');

        // Replace recipient@example.com with a "To" address. If your account 
        // is still in the sandbox, this address must be verified.
        // Also note that you can include several addAddress() lines to send
        // email to multiple recipients.
        $mail->addAddress('success@simulator.amazonses.com', 'David Moll');

        // Tells PHPMailer to send HTML-formatted email
        $mail->isHTML(true);

        // The subject line of the email
        $mail->Subject = 'Amazon SES test (SMTP interface accessed using PHP)';

        // The HTML-formatted body of the email
        $mail->Body    = '<h1>Email Test</h1>
            <p>This email was sent through the 
            <a href="http://aws.amazon.com/ses/">Amazon SES</a> SMTP
            interface using the <a href="https://github.com/PHPMailer/PHPMailer">
            PHPMailer</a> class.</p>';

        // The alternative email body; this is only displayed when a recipient
        // opens the email in a non-HTML email client. The \r\n represents a 
        // line break.
        $mail->AltBody = "Email Test\r\nThis email was sent through the 
        Amazon SES SMTP interface using the PHPMailer class.";

        if(!$mail->send()) {
            echo 'Email not sent.\r\n';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            echo '\r\n';
            $this->assertTrue(FALSE);
        } else {
            echo 'Email sent!';
            $this->assertTrue(TRUE);
        }
    }
}
