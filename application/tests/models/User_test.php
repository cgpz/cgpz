<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_test
 *
 * @author dmoll
 */
class User_test extends TestCase{
        private $user;
    
    public function setUp() {
        $this->user = new User();
        $this->userIds = array();
    }
    
    public function test_InsertUser()
    {
        $userData = array(
                'username' => 'test user',
                'email' => 'test@test.com'
            );
        
        $insert = $this->user->insert($userData);
        
        $this->assertTrue($insert > 0);
        
        echo "Created user with id " .$insert. " in the database. \r\n";
        array_push($this->userIds, $insert);
        
    }
    
    public function test_VerifyUserPassword()
    {
        $userData = array(
            'username' => 'test user',
            'email' => 'test23@test.com', 
            'password' => password_hash('password', PASSWORD_DEFAULT)
            //TODO Random avatar
        );
          
        $inserted_user = $this->user->insert($userData);
        $this->assertTrue($inserted_user > 0);
        
        // store the user_id we just created in the DB
        array_push($this->userIds, $inserted_user);
        
        // now retrieve the user from the DB and verify that the pw matches
        $db_parms['returnType'] = 'single';
        $db_parms['conditions'] = array(
            'email' => 'test23@test.com'
        );
        
        $userInfo = $this->user->getRows($db_parms);
        
        foreach($userInfo as $key => $value)
        {
            echo "retrieved " .$key. " : " .$value. " for " .$userData['email']."\r\n";
        }
        
        $passwordVerified = password_verify('password', $userInfo['password']);
        
        $this->assertTrue($passwordVerified);
    }
    
    function test_GetUser()
    {
        $userData = array(
                'username' => 'test user',
                'email' => 'test@test.com',                
            );
        
        $insert = $this->user->insert($userData);
        
        $this->assertTrue($insert > 0);
        
        echo "Created user with id " .$insert. " in the database. \r\n";
        array_push($this->userIds, $insert);
        
        //$retrievedUserData = $this->user->getById($insert);
        $retrievedUserData = $this->user->getRows(array('user_id'=>$insert));
        echo "test_GetUser output\n";
        foreach($retrievedUserData as $key => $value)
        {
            echo "retrieved " .$key. " : " .$value. " for " .$userData['email']."\r\n";
        }
        
        $this->assertEquals($userData['username'], $retrievedUserData['username']);
    }
    
    function test_AddAdmin()
    {
        $userData = array(
                'username' => 'test user',
                'email' => 'test@test.com', 
                'active' => 1,
                'is_admin' => 1                
            );
        
        $insert = $this->user->insert($userData);
        
        $this->assertTrue($insert > 0);
        
        echo "Created user with id " .$insert. " in the database. \r\n";
        array_push($this->userIds, $insert);
        
        //$retrievedUserData = $this->user->getById($insert);
        $retrievedUserData = $this->user->getRows(array('user_id'=>$insert));
        echo "test_GetUser output\n";
        foreach($retrievedUserData as $key => $value)
        {
            echo "retrieved " .$key. " : " .$value. " for " .$userData['email']."\r\n";
        }
        
        $this->assertEquals($userData['username'], $retrievedUserData['username']);
        $this->assertTrue($retrievedUserData['is_admin'] == 1);
    }
    
    public function test_UpdateUser()
    {
        $userData = array(
            'username' => 'test user',
            'email' => 'test@test.com', 
            'active' => 1,
            'is_admin' => 1                
        );
        
        $insert = $this->user->insert($userData);
        
        $this->assertTrue($insert > 0);
        
        echo "Created user with id " .$insert. " in the database. \r\n";
        array_push($this->userIds, $insert);
        
        $updatedUserData = array(
            'user_id' => $insert,
            'username' => 'updated',
            'email' => 'updated@emailtest.com', 
            'active' => 1,
            'is_admin' => 0                
        );
        
        $updated = $this->user->updateUser($updatedUserData);
               
        $this->assertTrue($updated == 1);
        
        // check that hte info was updated
        $userDataFromDB = $this->user->getRows(array('user_id'=>$insert));
        $this->assertEquals($updatedUserData['username'], $userDataFromDB['username']);
        $this->assertTrue($userDataFromDB['is_admin'] == 0);
        
    }
    
    public function tearDown()
    {
        foreach($this->userIds as $value)
        {
            echo "Deleting user with id " .$value. " from the database \r\n";
            $deleted = $this->user->deleteUser($value);
            
            echo $deleted."\r\n";
        }
    }
}
