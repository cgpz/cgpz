<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico">

    <title>Class Game Project ZoNe</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url(); ?>assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?php echo base_url(); ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url(); ?>assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="create-dialog" title="Create a game session">
    <form action="lobby/create" method="post">
        <label class="form-inline">
            Room name :
            <input type="text" class="form-control" name="title" value="<?php echo $user['username'] . '\'s session'?>">
        </label>
        <label class="form-inline">
            Game :
            <select name="game-id" class="form-control">
                <?php foreach ($games as $game):?>
                    <option value="<?php echo $game->game_id?>">
                        <?php echo $game->title?>
                    </option>
                <?php endforeach; ?>
            </select>
        </label>
        <input type="submit" value="Create Room" class="action ui-button form-control">
    </form>
</div>
<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <img class="masthead-brand" style="width: 16%; " src="<?php echo base_url(); ?>assets/img/logo.png"/>
                    <nav>
                        <ul class="nav masthead-nav">
                            <?php
                            if(!empty($user['is_admin']) && $user['is_admin'] == 1){
                                echo '<li class=""><a href="admin">Admin</a></li>';
                            }
                            ?>                            
			<li class="shop-link"><a href="shop">Shop</a></li>
                            <li class="active"><a href="#">Game Lobby</a></li>
                            <li class=""><a href="profile">Profile</a></li>
                            <li class=""><a href="lobby/logout">Logout</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php
                if(!empty($error_msg)){
                    echo '<p class="statusMsg">'.$error_msg.'</p>';
                }
            ?>
            <div class="inner cover lobby">
                <span id="newgame" class="action ui-button" style="position: absolute; right: 20px; float: right;">New game room</span>
                <h2>Join a Game</h2>
                <div class="session-list box">
                    <ul>
                        <?php if(!$game_sessions) echo 'No sessions found.'?>
                        <?php foreach ($game_sessions as $gs):?>
                            <li>
                                <?php echo $gs->title . ' ( ' . $gs->gtitle .' )'?>
                                <a href="lobby/join/<?php echo $gs->session_id?>"
                                class="ui-button action">
                                    Join
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Copyright 2017. Cover Theme for Bootstrap by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lobby.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
