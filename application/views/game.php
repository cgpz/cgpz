            <div class="">
                <canvas id="canvas" width="800" height="600"></canvas>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Copyright 2017. Cover Theme for Bootstrap by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                </div>
            </div>

        </div>

    </div>
    <span id="game-data" style="display: none";
          data-session="<?php echo $session_id?>"
          data-user="<?php echo $user['username']?>"
          data-sId="<?php echo $user['user_id']?>"
          data-av="<?php echo $avatar->filename?>"></span>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/game.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
