<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico">

    <title>Class Game Project ZoNe</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url(); ?>assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?php echo base_url(); ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url(); ?>assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <img class="masthead-brand" style="width: 16%;" src="<?php echo base_url(); ?>assets/img/logo.png"/>
                    <nav>
                        <ul class="nav masthead-nav">
                            <?php
                            if(!empty($user['is_admin']) && $user['is_admin'] == 1){
                                echo '<li class=""><a href="admin">Admin</a></li>';
                            }
                            ?>
                            <li class="shop-link active"><a href="shop">Shop</a></li>
                            <li class=""><a href="lobby">Game Lobby</a></li>
                            <li class=""><a href="profile">Profile</a></li>
                            <li class=""><a href="lobby/logout">Logout</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">

                <div class="shop-list">
                    <?php foreach ($avatars as $av):?>
                        <div class="shop-item box">
                            <img src="<?php echo base_url(); ?>assets/img/<?php echo $av->filename ?>"
                                 class="avatar <?php if($av->price > 0) { echo 'premium';} ?>"
                                 />
                            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_blank">
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="hosted_button_id" value="7MS6FUFJNRYZW">
                                <input type="hidden" name="avatar_id" value="<?php echo $av->avatar_id; ?>">

                                <input type='hidden' name='return' value='<?php echo base_url(); ?>index.php/shop/success'>
                                <input type='hidden' name='cancel_return' value='<?php echo base_url(); ?>index.php/shop'>

                                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                            </form>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="mastfoot">
                <div>
                    <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppppcmcvdam.png" alt="Pay with PayPal, PayPal Credit or any major credit card" />
                </div>
                <div class="inner">
                    <p>Copyright 2017. Cover Theme for Bootstrap by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/shop.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
