<h2><?php echo $title; ?></h2>
 
<?php echo validation_errors(); ?>
<?php echo form_open('Admin/edit_avatar/'.$avatar_data->avatar_id); ?>
    <table>
        <?php foreach($avatar_data as $key => $value): ?>
        <tr>
            <td><label><?php echo $key; ?></label></td>
            <td><input style="color: #000000;" type="text" name="<?php echo $key; ?>" size="50" value="<?php echo $value ?>" /></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td></td>
            <td><input id="edit_avatar" type="submit" name="edit" value="Save avatar"/></td>
        </tr>
    </table>    
</form>