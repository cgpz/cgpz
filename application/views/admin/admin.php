            <div id="admin" class="inner cover">
                <h2>User Information</h2> 
                    <table id="user_admin" border='2' cellpadding='5' margin='5'>
                        <tr>
                            <td><strong>user_id</strong></td>
                            <td><strong>username</strong></td>
                            <td><strong>email</strong></td>
                            <td><strong>created</strong></td>
                            <td><strong>active</strong></td>
                            <td><strong>avatar_id</strong></td>
                            <td><strong>is_admin</strong></td>
                            <td><strong>Actions</strong></td>
                        </tr>
                        <?php foreach ($site_users as $site_user): ?>
                                <tr>
                                    <td><?php echo $site_user['user_id']; ?></td>
                                    <td><?php echo $site_user['username']; ?></td>
                                    <td><?php echo $site_user['email']; ?></td>
                                    <td><?php echo $site_user['created']; ?></td>
                                    <td><?php echo $site_user['active']; ?></td>
                                    <td><?php echo $site_user['avatar_id']; ?></td>
                                    <td><?php echo $site_user['is_admin']; ?></td>
                                    <td>                                        
                                        <a href="<?php echo site_url('admin/edit_user/'.$site_user['user_id']); ?>">Edit</a> | 
                                        <a href="<?php echo site_url('admin/delete_user/'.$site_user['user_id']); ?>" onClick="return confirm('Are you sure you want to delete this user?')">Delete</a>
                                    </td>
                                </tr>
                        <?php endforeach; ?>
                    </table>      
                <?php
                if(!empty($user_success_msg)){
                    echo '<p class="statusMsg">'.$success_msg.'</p>';
                }elseif(!empty($user_error_msg)){
                    echo '<p class="statusMsg">'.$error_msg.'</p>';
                }
                ?>
                <h2>Avatar Information</h2>                
                <table id="avatar_admin" border='2' cellpadding='5' cellmargin='5'>
                    <tr>
                        <td><strong>avatar_id</strong></td>
                        <td><strong>filename</strong></td>
                        <td><strong>price</strong></td>
                        <td><strong>image</strong></td>
                        <td><strong>Actions</strong></td>
                    </tr>
                        <?php foreach ($site_avatars as $site_avatar): ?>
                                <tr>
                                    <td><?php echo $site_avatar->avatar_id; ?></td>
                                    <td><?php echo $site_avatar->filename; ?></td>
                                    <td><?php echo $site_avatar->price; ?></td>
                                    <td><img class="avatar" 
                                             src="<?php echo base_url(); ?>assets/img/<?php echo $site_avatar->filename ?>"/>
                                    </td>
                                    <td>                                        
                                        <a href="<?php echo site_url('admin/edit_avatar/'.$site_avatar->avatar_id); ?>">Edit</a> | 
                                        <a href="<?php echo site_url('admin/delete_avatar/'.$site_avatar->avatar_id); ?>" onClick="return confirm('Are you sure you want to delete this avatar?')">Delete</a>
                                    </td>
                                </tr>
                        <?php endforeach; ?>
                </table>
                <form method="POST" action="admin/add_avatar">
                <input type="submit" value="Add Avatar" name="add_avatar"/>
                </form>
                
            </div>
