<!-- Drawing on the canvas taken from 
    https://stackoverflow.com/questions/2368784/draw-on-html5-canvas-using-a-mouse -->

<script type="text/javascript">
    var canvas, ctx, flag = false,
        prevX = 0,
        currX = 0,
        prevY = 0,
        currY = 0,
        dot_flag = false;

    var x = "black",
        y = 2;
    
    function init() {
        canvas = document.getElementById('can');
        ctx = canvas.getContext("2d");
        w = canvas.width;
        h = canvas.height;
        
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    
        canvas.addEventListener("mousemove", function (e) {
            findxy('move', e)
        }, false);
        canvas.addEventListener("mousedown", function (e) {
            findxy('down', e)
        }, false);
        canvas.addEventListener("mouseup", function (e) {
            findxy('up', e)
        }, false);
        canvas.addEventListener("mouseout", function (e) {
            findxy('out', e)
        }, false);
    }
    
    function color(obj) {
        switch (obj.id) {
            case "green":
                x = "green";
                break;
            case "blue":
                x = "blue";
                break;
            case "red":
                x = "red";
                break;
            case "yellow":
                x = "yellow";
                break;
            case "orange":
                x = "orange";
                break;
            case "black":
                x = "black";
                break;
            case "white":
                x = "white";
                break;
        }
        if (x == "white") y = 14;
        else y = 2;
    
    }
    
    function draw() {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(currX, currY);
        ctx.strokeStyle = x;
        ctx.lineWidth = y;
        ctx.stroke();
        ctx.closePath();
    }
    
    function erase() {
        var m = confirm("Want to clear");
        if (m) {
            ctx.clearRect(0, 0, w, h);
            document.getElementById("canvasimg").style.display = "none";
        }
    }
    
    function save() {
        document.getElementById("canvasimg").style.border = "2px solid";
        var dataURL = canvas.toDataURL();
        document.getElementById("canvasimg").src = dataURL;
        document.getElementById("canvasimg").style.display = "inline";
    }
    
    function findxy(res, e) {
        if (res == 'down') {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
    
            flag = true;
            dot_flag = true;
            if (dot_flag) {
                ctx.beginPath();
                ctx.fillStyle = x;
                ctx.fillRect(currX, currY, 2, 2);
                ctx.closePath();
                dot_flag = false;
            }
        }
        if (res == 'up' || res == "out") {
            flag = false;
        }
        if (res == 'move') {
            if (flag) {
                prevX = currX;
                prevY = currY;
                currX = e.clientX - canvas.offsetLeft;
                currY = e.clientY - canvas.offsetTop;
                draw();
            }
        }
    }
    </script>
<body onload="init()">
<p>Draw pictures and have your friends guess what you're drawing!</p>
<form id="gameSelect" action="">
    <p>
        <label>
            <input style="color: #000000;" value="Begin" type="button"/>
        </label>
        <br/>
    </p>
</form>
<canvas id="can" width="500" height="500"
        style="border: 1px solid #c3c3c3;">
    Your browser does not support the canvas element.
</canvas>

<table style='margin-left: 10em; margin-right: 10em; padding: 1em'>
    <tr>
        <td>
            <div >Choose Color</div>
        </td>
        <td>
            <div style="border:10;margin:5;padding:5;float:left;width:10px;height:10px;background:green;" id="green" onclick="color(this)"></div>                
            <div style="border:10;margin:5;padding:5;float:left;width:10px;height:10px;background:blue;" id="blue" onclick="color(this)"></div>                
            <div style="border:10;margin:5;padding:5;float:left;width:10px;height:10px;background:red;" id="red" onclick="color(this)"></div>                
            <div style="border:10;margin:5;padding:5;float:left;width:10px;height:10px;background:yellow;" id="yellow" onclick="color(this)"></div>                
            <div style="border:10;margin:5;padding:5;float:left;width:10px;height:10px;background:orange;" id="orange" onclick="color(this)"></div>                   
            <div style="border:10;margin:5;padding:5;float:left;width:10px;height:10px;background:black;" id="black" onclick="color(this)"></div>
        </td>
    </tr>
    <tr>
        <td><div >Eraser</div></td>
        <td>
            <div style="width:15px;height:15px;background:white;border:2px solid;" id="white" onclick="color(this)"></div>
        </td>
    </tr>
    <tr>
        <td><input type="button" value="save" id="btn" size="30" onclick="save()" style="color: #000000;"></td>
        <td><input type="button" value="clear" id="clr" size="23" onclick="erase()" style="color: #000000;"></td>
    </tr>
<img id="canvasimg" style="display:none;">
</body>
</html>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
