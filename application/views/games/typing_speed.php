
<body>
<p>Let's test your typing speed!</p>
<p>Type the words as they appear.</p>
<form id="gameSelect" action="">
    <p>
        <label>
            <input style="color: #000000;" value="Begin" type="button"/>
        </label>
        <br/>
    </p>
</form>
<canvas id="myCanvas" width="500" height="500"
        style="border: 1px solid #c3c3c3;">
    Your browser does not support the canvas element.
</canvas>
</body>
</html>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
