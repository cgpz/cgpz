
<body onload="initalizeBoard()">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/MoveList.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moveProcessing.js">
</script>
<p>Let's play the peg game!  White pegs are empty pegs.</p>
<p>The "Random Start" button will reset the board with a random empty peg slot.</p>
<form id="gameSelect" action="">
    <p>
        <label>
            <input style="color: #000000;" value="Reset Board" onclick="resetBoard()" type="button"/>
        </label>
        <label>
            <input style="color: #000000;" value="Random Start" onclick="randomStart()" type="button" />
        </label>
        <br/>
    </p>
</form>
<canvas id="myCanvas" width="500" height="500"
        style="border: 1px solid #c3c3c3;"
        onmousedown="canvasClicked(event)"
        onmouseup="dropCircle(event)">
    Your browser does not support the canvas element.
</canvas>
<p id="gameOverDisplay"></p>
<p id="moveInfo"></p>
<p id="movesRemaining"></p>
<p id="circleOneInfo"></p>
<p id="circleClicked"></p>
</body>
</html>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/peg_game.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
