<?php
/**
 * Control sending of Emails
 *
 * @author dmoll
 */

class Email extends CI_Model
{    
    function __construct()
    {
        parent::__construct();             
    }
    
    /*
     * Create a random, unique code that will be emailed
     * to a user after being stored in the database when the user
     * registers for an account.
     * 
     * The user clicks a link with this code to activate their account.
     */
    function generateEmailVerificationCode()
    {
        $this->load->helper('string');
        $verificationCode = random_string('sha1', 40);

        return $verificationCode;
    }
    
    /*
     * Code adapted from AWS documents on sending emails through AWS SES
     */
    function sendVerificationEmail($emailAddress, $verificationCode)
    {
        $verificationUrl = base_url() . "index.php/auth/verify/" . $verificationCode;
        
        $emailBody = "Welcome to Class Game Project Zone!\n";
        $emailBody .= "Please click on below URL or paste into your browser to verify your Email Address";
        $emailBody .= "\n\n" .$verificationUrl. "\n"."\n\nThanks!\nYour friendly CGPZ Admins.";
        
        // Instantiate a new PHPMailer 
        $mail = new PHPMailer;

        // Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //$mail->SMTPDebug = 2;

        // If you're using Amazon SES in a Region other than US West (Oregon), 
        // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
        // endpoint in the appropriate Region.
        $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        // Tells PHPMailer to use SMTP authentication
        $mail->SMTPAuth = true;

        // Replace smtp_username with your Amazon SES SMTP user name.
        $mail->Username = 'AKIAIP2LFU57E7PBO5IA';

        // Replace smtp_password with your Amazon SES SMTP password.
        $mail->Password = 'Ai44sRPn3yzNdWVNNEFoQavMVfLufno8Af/HvAOaf0Fd';

        // Enable SSL encryption
        $mail->SMTPSecure = 'ssl';

        // The port you will connect to on the Amazon SES SMTP endpoint.
        $mail->Port = 465;

        // Replace sender@example.com with your "From" address. 
        // This address must be verified with Amazon SES.
        $mail->setFrom('dmoll@emich.edu', 'David Moll');

        // Replace recipient@example.com with a "To" address. If your account 
        // is still in the sandbox, this address must be verified.
        // Also note that you can include several addAddress() lines to send
        // email to multiple recipients.
        $mail->addAddress($emailAddress);

        // Tells PHPMailer to send HTML-formatted email
        $mail->isHTML(true);

        // The subject line of the email
        $mail->Subject = 'Email Verification';

        // The HTML-formatted body of the email
        $mail->Body = $emailBody;

        // The alternative email body; this is only displayed when a recipient
        // opens the email in a non-HTML email client. The \r\n represents a 
        // line break.
        $mail->AltBody = $emailBody;

        if(!$mail->send()) {
//            echo 'Email not sent.\r\n';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//            echo '\r\n';      
            return FALSE;
        } else {
//            echo 'Email sent!';            
            return TRUE;
        }
    }
}
