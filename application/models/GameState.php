<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GameState extends CI_Model
{
    public function __construct()
    {
        $this->gameStateTbl = "game_state";
    }

    public function getState($sessionId){
        $query = $this->db->where('session_id', $sessionId)->get($this->gameStateTbl);

        return $query->result();
    }

    public function createState($sessionId, $userId){
        $params = array(
            'session_id' => $sessionId,
            'state' => json_encode(array('users' => array(), 'exec' => '', 'owner' => $userId))
        );
        if(!$this->getState($sessionId)) {
            $insert = $this->db->insert($this->gameStateTbl, $params);

            if($insert){
                return $this->db->insert_id();
            }else{
                return false;
            }
        }
    }

    public function saveState($params) {
        $data = array(
            'state' =>  json_encode($params->state)
        );
        $this->db->where('session_id', $params->session_id)
            ->update($this->gameStateTbl, $data);

    }

}