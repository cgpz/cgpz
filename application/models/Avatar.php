<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Avatar extends CI_Model
{
    public function __construct()
    {
        $this->avatarTbl = 'avatar';
        $this->userTbl = 'user';
        $this->premiumTbl = 'user_premium_avatar';
        
        $this->avatarId = 'avatar_id';
    }

    public function getAll() {
        $query = $this->db->get($this->avatarTbl);
        return $query->result();
    }

    public function getPremium() {
        $query = $this->db->where('price >', 0)
                            ->get($this->avatarTbl);

        return $query->result();
    }

    public function getAvailableAvatarsForUser($id) {
        $query = $this->db->select($this->avatarTbl . '.*')
                            ->from($this->avatarTbl)
                            ->join($this->premiumTbl, $this->premiumTbl . '.avatar_id = ' . $this->avatarTbl . '.avatar_id', 'left')
                            ->join($this->userTbl, $this->premiumTbl . '.user_id = ' . $this->userTbl . '.user_id', 'left')
                            ->where($this->userTbl . '.user_id', $id)
                            ->or_where($this->avatarTbl . '.price', 0)
                            ->get();

        return $query->result();
    }

    public function getUserPremium($id){

        $query = $this->db->select($this->avatarTbl. '.avatar_id')
                            ->from($this->avatarTbl)
                            ->join($this->premiumTbl, $this->premiumTbl . '.avatar_id = ' . $this->avatarTbl . '.avatar_id')
                            ->where($this->premiumTbl . '.user_id', $id)
                            ->get();

        return $query->row_array();
    }


    public function getAvailablePremiumForUser($id) {
        $avs = $this->getUserPremium($id);
        if($avs) {
            $query = $this->db->select($this->avatarTbl. '.*')
                ->from($this->avatarTbl)
                ->where_not_in($this->avatarTbl . '.avatar_id', $avs)
                ->where($this->avatarTbl . '.price >', 0)
                ->get();
        } else {
            $query = $this->db->select($this->avatarTbl. '.*')
                ->from($this->avatarTbl)
                ->where($this->avatarTbl . '.price >', 0)
                ->get();
        }
        return $query->result();
    }

    /*
     * Returns avatar object for given user
     */
    public function getUserAvatar($userId) {
        $query = $this->db->select($this->avatarTbl . '.*')
                            ->from($this->avatarTbl)
                            ->join($this->userTbl, $this->userTbl. '.avatar_id = ' . $this->avatarTbl . '.avatar_id')
                            ->where($this->userTbl . '.user_id', $userId)
                            ->get();
        return $query->result()[0];
    }

    public function generateRandom() {
        //TODO get id values for free avatars
        return rand(1, 9);
    }

    public function getAvatarById($avatarId) 
    {
        $query = $this->db->where($this->avatarId, $avatarId)
                            ->get($this->avatarTbl);

        return $query->result()[0];
    }
    
    public function addAvatar($fileName, $price)
    {
        $avatarData = array(
                'filename' => $fileName,
                'price' => $price
        );
        
        //insert user data to users table
        $insert = $this->db->insert($this->avatarTbl, $avatarData);

        //return the status
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }

    public function addPremiumAvatarForUser($userId, $avatarId){
        $data = array(
            'avatar_id' => $avatarId,
            'user_id' => $userId,
            'created' => time()
        );
        //insert user data to users table
        $insert = $this->db->insert($this->premiumTbl, $data);

        //return the status
        if($insert){
            return true;
        }else{
            return false;
        }
    }
}