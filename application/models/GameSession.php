<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GameSession extends CI_Model
{
    public function __construct()
    {
        $this->avatarTbl = 'avatar';
        $this->userTbl = 'user';
        $this->gameTbl = 'game';
        $this->gameSessionTbl  = 'session';
    }

    public function getCurrentSessions(){
        $query = $this->db->select($this->gameSessionTbl . '.*, ' . $this->gameTbl . '.title  as `gtitle`')
                            ->from($this->gameSessionTbl)
                            ->join($this->gameTbl, $this->gameTbl . '.game_id = '. $this->gameSessionTbl . '.game_id')
                            ->where('expired ', 0)
                            ->get();
        return $query->result();
    }

    public function getGames() {
        $query = $this->db->get($this->gameTbl);
        return $query->result();
    }

    public function getSession($id){
        $query = $this->db->where('session_id', $id)
                            ->get($this->gameSessionTbl);
        return $query->result();
    }

    public function createGameSession($params = array()) {

        $insert = $this->db->insert($this->gameSessionTbl, $params);
        //return the status
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
}