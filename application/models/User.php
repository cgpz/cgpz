<?php 
class User extends CI_Model{
    function __construct() {
        $this->userTbl = 'user';
        $this->userId = 'user_id';
    }
    /*
     * get rows from the users table
     */
    function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->userTbl);

        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        //If the query parameters include a search by id
        if(array_key_exists($this->userId, $params)){
            $this->db->where($this->userId,$params[$this->userId]);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //if the parameters include limits for the results
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            //get the results
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        //return fetched data
        return $result;
    }

    
    public function getById($user_id)
    {
        $query = $this->db->select($this->userTbl . '.*')
                    ->from($this->userTbl)
                    ->where($this->userTbl . '.user_id', $user_id)
                    ->get();
        
        return $query->result()[0];
    }
    
    /*
     * Insert user information
     */
    public function insert($data = array()) {
        //add created and modified data if not included
        if(!array_key_exists("created", $data)){
            $data['created'] = date("Y-m-d H:i:s");
        }

        //insert user data to users table
        $insert = $this->db->insert($this->userTbl, $data);

        //return the status
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }
    
    /*
     * 
     */
    public function verifyEmailAddress($verificationCode)
    {
        // try to update the active flag to 1
        // for the database row with the given email_verification_code code
        
        $db_parms = array(
            'active' => 1
        );

        $this->db->where('email_verification_code', $verificationCode);
        $this->db->update($this->userTbl, $db_parms);
        
        $updateResult = $this->db->affected_rows();
        
        return $updateResult;
    }
    
    /*
     * Deletes a user from the database
     * 
     * @param string $user_id Primary key of the user to be deleted
     * @return int 1 if deleted, 0 if not deleted.
     */
    public function deleteUser($user_id)
    {
        $this->db->delete($this->userTbl, array('user_id' => $user_id));        
        $deleted = $this->db->affected_rows();
        
        return $deleted;
    }

    public function setUserAvatar($userId, $avatarId) {
        $data = array(
            'avatar_id' => $avatarId,
        );

        $this->db->where('user_id', $userId);
        $this->db->update($this->userTbl, $data);
    }

    /*
     * Updates a user in the database.
     * @param array $params An associate array containing the row in the db to be updated.
     * This must contain a user_id key with the associated value to perform the update.      
     * @return int 1 if the row was updated, 0 if no rows were updated.
     */
    public function updateUser($params)
    {
        if(array_key_exists($this->userId, $params))
        {
            $userId = $params['user_id'];
            unset($params['user_id']);
            
            $this->db->where($this->userId, $userId);
            $this->db->update($this->userTbl, $params);
                    
            $updateResult = $this->db->affected_rows();
            
            return $updateResult;
        }        
        else
        {
            return 0;
        }
        
    }
}