<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('gameSession');
        $this->load->model('gameState');
        $this->load->model('avatar');
    }

    public function index() {
        $data = array();
        if($this->session->userdata('isUserLoggedIn')){
            if($this->session->userdata('game_session_id')) {
                $params = array(
                    'user_id' => $this->session->userdata('userId')
                );
                $data['user'] = $this->user->getRows($params);
                $data['game_id'] = $this->gameSession->getSession($this->session->userdata('game_session_id'));
                $data['avatar'] = $this->avatar->getUserAvatar($params['user_id']);
                $data['session_id'] = $this->session->userdata('game_session_id');
                $gameS = $this->gameSession->getSession($data['session_id'])[0];
                $insert = $this->gameState->createState($data['session_id'], $gameS->user_id);
                
                $game_view = $this->getGameView($data['game_id']);
                
                $this->load->view('templates/game_header', $data);
                $this->load->view($game_view, $data);
                $this->load->view('templates/game_footer', $data);
                
            } else {
                redirect('lobby');
            }
        } else {
            redirect('');
        }
    }

    public function get() {
        $session_id = $this->input->post('gameSessionId');
        $user = $this->input->post('user');
        $av = $this->input->post('av');
        $req = $this->input->post('req');
        $id = $this->input->post('id');
        $mu = $this->input->post('mu');
        $gameS = $this->gameSession->getSession($session_id)[0];

        if(!$gameS->expired) {
            $state = $this->gameState->getState($session_id)[0];
            $state->state = json_decode($state->state, true);

            //new user
            if($req == 'nU') {
                //Add new user to given session or reset the user
                $state->state['users'][$id] = array(
                    'av' => $av,
                    'user' => $user,
                    'exec' => 'nU$' . $user . '$' . 'id',
                    'ping' => time()
                );
                //TODO create session stats for user
                //echo 'User ' . $user . ' added for game session '. $session_id
                //    . ' with '. count($state->state['users']) . ' other users';
                //echo var_dump($state);

                echo json_encode($state);
                $this->gameState->saveState($state);

            }
            //ready
            if($req == 'rU') {
                $state->state['users'][$id]['exec'] = 'rU$' . $id;
                $this->gameState->saveState($state);
                echo json_encode($state);
            }
            //new move
            if($req == 'mU') {
                $state->state['users'][$id]['exec'] = 'mU$' . $mu;
                $this->gameState->saveState($state);
                echo json_encode($state);
            }
            //check
            if($req == 'cc') {
                $state->state['users'][$id]['ping'] = time();
                $this->gameState->saveState($state);
                echo json_encode($state);
            }
        }

    }
    
    /*
     * Load a different view based on the game id
     * Normally we'd have a column in the database with the name
     * of the game view, but in the interest of time let's hard code values.
     * 
     * @param array $pageData Array of data created when user selects a game
     * @return string The relative path to the view for the selected game
     */
    public function getGameView($pageData)
    {
        $game_id = 0;
       
        if(count($pageData) == 1)
        {
            $gameData = $pageData[0];

            $game_id = $gameData->game_id;
        }
        
        $game_view = '';
        
        switch($game_id) :
            case 1 :
                $game_view = 'games/typing_speed';
                break;
            case 2 :
                $game_view = 'game';
                break;
            case 3 :
                $game_view = 'games/draw_it';
                break;
            case 4 :
                $game_view = 'games/describe_it';
                break;
            case 5 :
                $game_view = 'games/pegs';
                break;
            default :
                $game_view = 'game';
        endswitch;
        
        return $game_view;
    }
}