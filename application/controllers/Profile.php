<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('avatar');
        $this->load->model('gameSession');
    }

    public function index() {
        if($this->session->userdata('isUserLoggedIn')){
            $params = array(
                'user_id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->user->getRows($params);
            $data['avatar'] = $this->avatar->getUserAvatar($params['user_id']);
            $data['avatars'] = $this->avatar->getAvailableAvatarsForUser($params['user_id']);
            $data['games'] = $this->gameSession->getGames();
            $this->load->view('users/profile', $data);
        } else {
            redirect('');
        }
    }

    public function update($avatarId) {
        if($avatarId) {
            $this->user->setUserAvatar($this->session->userdata('userId'), $avatarId);
            redirect("profile");
        }
    }
}