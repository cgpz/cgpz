<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author dmoll
 */
class Admin extends CI_Controller{
    //put your code here
    
    public function __construct(){           
        parent::__construct();
        $this->load->model('user');
        $this->load->model('game');
        $this->load->model('avatar');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');        
    }
    
    public function index()
    {
        $data = $this->session->flashdata();
        
        if($this->session->userdata('isUserLoggedIn')){
            
            // we have to recheck if the user is an admin
            $userData = $this->user->getRows(array('user_id'=>$this->session->userdata('userId')));
            
            if($userData['is_admin'] == 1)
            {
                //load the view
                $data['user'] = $userData;
                
                // get the user data for the website
                $data['site_users'] = $this->user->getRows();
                $data['site_avatars'] = $this->avatar->getAll();
                
                $this->load->view('templates/admin_header', $data);
                $this->load->view('admin/admin', $data);
                $this->load->view('templates/admin_footer');
            }
        } else {
            // if the user isn't an admin, redirect back to index.php
            redirect('');
        }
    }
    
    public function edit_user()
    {
        $user_id = $this->uri->segment(3);
        
        if(empty($user_id))
        {
            show_404();
        }                
        
        $data['title'] = 'Edit user information';        
        $data['user_data'] = $this->user->getById($user_id);
        
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('avatar_id', 'avatar_id', 'required');
        $this->form_validation->set_rules('is_admin', 'is_admin', 'required');
 
        $postData = $this->input->post();
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/admin_header', $data);
            $this->load->view('admin/edit_user', $data);
            $this->load->view('templates/admin_footer');
        }
        else
        {
            // remove the value of the input button from the post data
            unset($postData['edit']);
            
            $updated = $this->user->updateUser($postData);
            //$this->load->view('news/success');
            
            if($updated == 1)
            {
                $data['user_success_msg'] = 'User '.$postData['user_id'].' updated successfully.';                
            }
            else
            {
                $data['user_error_msg'] = 'User '.$postData['user_id'].' could not be updated.';                
            }
            
            $this->session->set_flashdata($data);
            redirect( base_url() . 'index.php/admin');
 
        }
    }
   
    public function delete_user()
    {
        $user_id = $this->uri->segment(3);
        
        if(empty($user_id))
        {
            show_404();
        }
        
        $deleted = $this->user->deleteUser($user_id);
        
        if($deleted == 1)
        {
            $data['success_msg'] = 'User '.$user_id.' was deleted.';                
        }
        else
        {
            $data['error_msg'] = 'User '.$user_id.' could not be deleted.';                
        }
        
        $this->session->set_flashdata($data);
        redirect( base_url() . 'index.php/admin');
    }
    
    public function edit_avatar()
    {
        $avatar_id = $this->uri->segment(3);
        
        if(empty($avatar_id))
        {
            show_404();
        }               
        
        $data['title'] = 'Edit Avatar Information';        
        $data['avatar_data'] = $this->avatar->getAvatarById($avatar_id);
        
        $this->form_validation->set_rules('filename', 'filename', 'required');        
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('imagefile', 'imagefile', 'required');
 
        $postData = $this->input->post();
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/admin_header', $data);
            $this->load->view('admin/edit_avatar', $data);
            $this->load->view('templates/admin_footer');
        }
        else
        {
            // remove the value of the input button from the post data
            unset($postData['edit']);
            
            $updated = $this->user->updateAvatar($postData);
            //$this->load->view('news/success');
            
            if($updated == 1)
            {
                $data['avatar_success_msg'] = 'Avatar '.$postData['$avatar_id'].' updated successfully.';                
            }
            else
            {
                $data['avatar_error_msg'] = 'Avatar '.$postData['$avatar_id'].' could not be updated.';                
            }
            
            $this->session->set_flashdata($data);
            redirect( base_url() . 'index.php/admin'); 
        }
    }
    
    public function delete_avatar()
    {
        $avatar_id = $this->uri->segment(3);
        
        if(empty($avatar_id))
        {
            show_404();
        }
    }
    
    public function add_avatar()
    {
        $data['title'] = 'Upload an avatar image';

        $this->form_validation->set_rules('price', 'Price', 'required');        

        $config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);
        
        $postData = $this->input->post();
        
        if(! isset($postData['submit']))
        {
            $this->load->view('templates/admin_header', $data);
            $this->load->view('admin/add_avatar');
            $this->load->view('templates/admin_footer');
        }
        else
        {            
            if ( ! $this->upload->do_upload('userfile'))
            {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('admin/add_avatar', $error);
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                
                $price = $postData['price'];
                $fileName = $data['upload_data']['file_name'];
                
                $this->avatar->addAvatar($fileName, $price);                
            
                $this->load->view('templates/admin_header', $data);
                $this->load->view('admin/add_avatar_success', $data);
                $this->load->view('templates/admin_footer');
            }           
        }
    }
}
