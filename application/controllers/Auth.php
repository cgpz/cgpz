<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user');
	$this->load->model('email');
        $this->load->model('avatar');
    }

    public function index()
    {
        log_message('debug', 'Auth::index entered.');
        
        $data = array();
        if($this->session->userdata('isUserLoggedIn')){
            $data['user'] = $this->user->getRows(array('user_id'=>$this->session->userdata('userId')));
            //load the view
            redirect('profile');
        }else{
            $this->load->view('index');
        }
    }

    public function signup(){
        log_message('debug', 'Auth::signup entered.');
        
        $data = array();
        
        $data = $this->handleSessionMessages($data);
        
        $userData = array();
        if($this->input->post('regisSubmit')){
            $this->form_validation->set_rules('username', 'Username', 'required|callback_check_username');
            $this->form_validation->set_rules('email', 'Email', 'required|callback_check_email');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[password]');

            $userData = array(
                'username' => strip_tags($this->input->post('username')),
                'email' => strip_tags($this->input->post('email')),                
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'email_verification_code' => $this->email->generateEmailVerificationCode(),
                'avatar_id' => $this->avatar->generateRandom()
            );

            if($this->form_validation->run() == true){
                $insert = $this->user->insert($userData);
                if($insert){
                    $this->email->sendVerificationEmail($userData['email'], $userData['email_verification_code']);
                    $this->session->set_userdata('success_msg', 'You successfully signed up to join CGPZ. Please check your email for a confirmation message.');
                    log_message('debug', 'Auth::signup completed .'. implode(",",$userData));
                    redirect('auth/login');
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                    
                    log_message('debug', 'Auth::signup error .'.$data['error_msg'].implode(",",$userData));
                }
            }
        }
        $data['user'] = $userData;
        //load the view
        $this->load->view('users/signup', $data);
    }

    public function login(){
        log_message('debug', 'Auth::login entered.');
        
        $data = array();
        
        $data = $this->handleSessionMessages($data);
        
        if($this->input->post('loginSubmit')){
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            if ($this->form_validation->run() == true) {
                
                $con['returnType'] = 'single';
                $con['conditions'] = array(
                    'username'=>$this->input->post('username')
                );                            
                
                log_message('debug', 'Auth::login Trying to log in user: '.$this->input->post('username'));
                
                $checkLogin = $this->user->getRows($con);
                if($checkLogin){
                    
                    $passwordVerified = password_verify($this->input->post('password'), 
                            $checkLogin['password']);
                                        
                    log_message('debug', 'Auth::login Retrieved user pw hash: '.$checkLogin['password']);
                    log_message('debug', 'Auth::login Password verify returned: '.$passwordVerified);
                    
                    if($passwordVerified)
                    {
                        $this->session->set_userdata('isUserLoggedIn',TRUE);
                        log_message('debug', 'Auth::login Setting userdata to isUserLoggedin = true');
                        $this->session->set_userdata('userId',$checkLogin['user_id']);
			$this->session->set_userdata('username', $checkLogin['username']);
                        redirect('lobby');
                    }
                }else{
                    $data['error_msg'] = 'Wrong username or password, please try again.';
                    
                    log_message('debug', 'Auth::login error : '.$data['error_msg']);
                }
            }
        }
        //load the view
        $this->load->view('users/login', $data);
    }

    /*
     * Attempt to verify the user's registration.  If successful, redirect to the login
     * page.  If unsuccessful, redirect to the registration page.
     * 
     * @param string $verificationCode Unique code assigned to a user when registering      
     */
    public function verify($verificationCode=NULL)
    {
        $data = array();
        
        $userVerified = $this->user->verifyEmailAddress($verificationCode);        
        
        if($userVerified > 0)
        {           
            $this->session->set_userdata('success_msg', 'Email Verified Successfully!  You may now log in.');
            redirect('auth/login');    
            $this->load->view('users/login', $data);
        } else {            
            $this->session->set_userdata('error_msg', 'We were unable to verify your email address!  Please try to register again.');
            redirect('auth/signup');
            $this->load->view('users/signup', $data);            
        }

    }
    
    /*
     * Handle transferring success and error messages
     * between the session data and the $data object
     * the views use to display information.
     * 
     * This was used by login() and signup(), so it's been
     * extracted to its own function for simplicity.
     * 
     * Turns out we need to return $data after modifying it
     * to get the updates correct, even though it's an array.
     * 
     * @param array $data The key-value pairs of data for a view to display
     * @return array The key-value pairs of data updated from session vars
     */
    public function handleSessionMessages($data)
    {
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        
        return $data;
    }
    
    /*
     * Check that the email the user is registering with
     * doesn't already exist in the database.
     * 
     * @param string $email Email address to check against DB for uniqueness.
     * @return bool True if the email entered doesn't exist in the DB.
     */
    public function check_email($email)
    {
        $queryParameters['returnType'] = 'count';
        $queryParameters['conditions'] = array('email' => $email);
        
        $checkEmail = $this->user->getRows($queryParameters);
        
        if($checkEmail > 0)
        {
            $this->form_validation->set_message('check_email', 'This email is in use.  Please enter another.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    /*
     * Check that the name the user is registering with
     * doesn't already exist in the database.
     * 
     * @param string $username user name to check against DB for uniqueness.
     * @return bool True if the user name entered doesn't exist in the DB.
     */
    public function check_username($username)
    {
        $queryParameters['returnType'] = 'count';
        $queryParameters['conditions'] = array('username' => $username);
        
        $checkUserName = $this->user->getRows($queryParameters);
        
        if($checkUserName > 0)
        {
            $randomUserName = $this->getRandomUsername($username);
            $errorMsg = 'This username is in use.  Please try another, such as: ' . $randomUserName;
            $this->form_validation->set_message('check_username', $errorMsg);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    /*
     * Get a username for the user with a random numeric
     * string at the end.
     * 
     * @param string $username user name to append a random number to
     * @return string the user name with a random number at the end
     */
    public function getRandomUsername($username)
    {
        $checkUserName = 1;
        $randomDigits = 2;
        
        // we don't want to get stuck in an infinite loops if there are lots of users
        $maxAttempts = 10;
        
        // count how many times we have to search to get a good username
        $numAttempts = 0;
        
        while($checkUserName > 0)
        {
            $randSuffix = '';
            for($i=0; $i < $randomDigits; $i++)
            {
                $randSuffix.=rand(0,9);
            }

            $randomUserName = $username . $randSuffix;

            $queryParameters['returnType'] = 'count';
            $queryParameters['conditions'] = array('username' => $randomUserName);

            $checkUserName = $this->user->getRows($queryParameters);
            
            if($numAttempts > $maxAttempts)
            {
                $randomDigits += 1;
                $numAttempts = 0;
            }
        }
        
        return $randomUserName;
    }
}
