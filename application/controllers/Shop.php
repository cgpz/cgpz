<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('avatar');
    }

    public function index() {
        if($this->session->userdata('isUserLoggedIn')){
            $params = array(
                'user_id' => $this->session->userdata('userId')
            );
            $data['avatars'] = $this->avatar->getAvailablePremiumForUser($params['user_id']);
            $data['user'] = $this->user->getRows($params);
            $data['paypalID'] = 'cgpz@gmail.com';
            $this->load->view('shop', $data);
        } else {
            redirect('');
        }
    }

    public function success() {
        if($this->session->userdata('isUserLoggedIn')){
            $avatarId = $this->input->get('avatar_id');
            $userId = $this->session->userdata('userId');

            $this->avatar->addPremiumAvatarForUser($userId, $avatarId);
        } else {
            redirect('');
        }
    }
}