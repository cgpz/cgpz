<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lobby extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('gameSession');
    }

    public function index(){
        $data = array();
        if($this->session->userdata('isUserLoggedIn')){
            $data['user'] = $this->user->getRows(array('user_id'=>$this->session->userdata('userId')));
            $data['games'] = $this->gameSession->getGames();
            $data['game_sessions'] = $this->gameSession->getCurrentSessions();
            $this->load->view('lobby', $data);
        } else {
            redirect('');
        }
    }

    public function create() {
        $params = array(
            'game_id' => $this->input->post('game-id'),
            'title' => $this->input->post('title'),
            'user_id' => $this->session->userdata('userId'),
        );

        $insert = $this->gameSession->createGameSession($params);
        if($insert) {
            $this->session->set_userdata('game_session_id', $insert);
        } else {
            //$error_msg = 'Error creating a session for this game. Please retry later.';
            echo 'alert("Error joining this game session")';
            redirect('lobby');
        }
        redirect('game');
    }

    public function join($session_id) {
        //TODO Add Stats
        $this->session->set_userdata('game_session_id', $session_id);
        redirect('game');
    }

    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('game_session_id');
        $this->session->sess_destroy();
        redirect('');
    }
}