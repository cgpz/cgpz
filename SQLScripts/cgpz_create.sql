-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cgpz
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avatar`
--

DROP TABLE IF EXISTS `avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avatar` (
  `avatar_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `price` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`avatar_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avatar`
--

LOCK TABLES `avatar` WRITE;
/*!40000 ALTER TABLE `avatar` DISABLE KEYS */;
INSERT INTO `avatar` VALUES (1,'1.jpg',0),(2,'2.jpg',0),(3,'3.jpg',0),(4,'4.jpg',0),(5,'5.jpg',0),(6,'6.jpg',0),(7,'7.jpg',0),(8,'8.jpg',10),(9,'9.jpg',10),(10,'10.jpg',10),(11,'11.jpg',10);
/*!40000 ALTER TABLE `avatar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` VALUES (1,'Typing Speed Competition'),(2,'Hangman'),(3,'Draw It!'),(4,'Describe It!'),(5,'Peg Solitaire');
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_state`
--

DROP TABLE IF EXISTS `game_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_state` (
  `session_id` int(11) NOT NULL,
  `state` longtext,
  PRIMARY KEY (`session_id`),
  CONSTRAINT `fk_game_state_session1` FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_state`
--

LOCK TABLES `game_state` WRITE;
/*!40000 ALTER TABLE `game_state` DISABLE KEYS */;
INSERT INTO `game_state` VALUES (2,'{\"users\":{\"\":{\"av\":\"2.jpg\",\"user\":\"geth\",\"ping\":1497975425},\"1\":{\"av\":\"2.jpg\",\"user\":\"geth\",\"exec\":\"nU$geth$id\",\"ping\":1497986068},\"2\":{\"av\":\"8.jpg\",\"user\":\"geth2\",\"exec\":\"nU$geth2$id\",\"ping\":1497985427}},\"exec\":\"nU$geth$id\"}'),(3,'{\"users\":{\"1\":{\"av\":\"2.jpg\",\"user\":\"geth\",\"exec\":\"nU$geth$id\",\"ping\":1497990169},\"2\":{\"av\":\"8.jpg\",\"user\":\"geth2\",\"exec\":\"nU$geth2$id\",\"ping\":1497990185}},\"exec\":\"\"}'),(4,'{\"users\":{\"1\":{\"av\":\"2.jpg\",\"user\":\"geth\",\"exec\":\"nU$geth$id\",\"ping\":1498038534},\"2\":{\"av\":\"8.jpg\",\"user\":\"geth2\",\"exec\":\"nU$geth2$id\",\"ping\":1498022958},\"3\":{\"av\":\"3.jpg\",\"user\":\"geth3\",\"exec\":\"nU$geth3$id\",\"ping\":1498038924}},\"exec\":\"\",\"owner\":\"1\"}');
/*!40000 ALTER TABLE `game_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expired` tinyint(1) DEFAULT '0',
  `title` varchar(64) DEFAULT NULL,
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`session_id`),
  KEY `fk_game_has_user_user1_idx` (`user_id`),
  KEY `fk_game_has_user_game1_idx` (`game_id`),
  CONSTRAINT `fk_game_has_user_game1` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES (2,1,1,'geth\'s session',1),(2,1,1,'geth\'s session',2),(2,2,1,'geth2\'s session',3),(2,1,1,'geth\'s session',4);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_stats`
--

DROP TABLE IF EXISTS `session_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_stats` (
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stats` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`session_id`,`user_id`),
  KEY `fk_session_has_user_user1_idx` (`user_id`),
  KEY `fk_session_has_user_session1_idx` (`session_id`),
  CONSTRAINT `fk_session_has_user_session1` FOREIGN KEY (`session_id`) REFERENCES `session` (`session_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_session_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_stats`
--

LOCK TABLES `session_stats` WRITE;
/*!40000 ALTER TABLE `session_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `session_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `avatar_id` int(11) NOT NULL,
  `active` tinyint(4) DEFAULT '0',
  `email_verification_code` varchar(45) DEFAULT NULL,
  `is_admin` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_verification_code_UNIQUE` (`email_verification_code`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_user_avatar1_idx` (`avatar_id`),
  CONSTRAINT `fk_user_avatar1` FOREIGN KEY (`avatar_id`) REFERENCES `avatar` (`avatar_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES 
(1,'geth','dserguei91@gmail.com','$2y$10$LUB/lfua6KMflmLJwQFG2efftwmcu/rc540DtQUMdkDgRrfX3L6cO','2017-06-20 09:03:58',8,1,'912EC803B2CE49E4A541068D495AB570',1),
(2,'geth2','dserguei@hotmail.fr','$2y$10$QiQ9glfCe8mdKlLZwiwpt.osj1v.mc1iaMXC5tlnhP1WeI/G9j1gO','2017-06-20 09:22:26',8,1,'C81E728D9D4C2F636F067F89CC14862C',1),
(3,'geth3','dserguei9s1@gmail.com','$2y$10$8NenhElFURddr5Eo0LY37OStzzHx52ffScqWrQIaaQe0jbiuFWwWu','2017-06-21 03:40:17',3,1,'E4DA3B7FBBCE2345D7772B0674A318D5',1),
(56,'dmoll','mollerwa4@gmail.com','$2y$10$lf0Exfu0OHUEk/b.j4AIreD2LTaDMhymuO5rccUMbZIMRUYc/q9HK','2017-06-18 15:51:46',11,1,'37dda29de7a0f9157697eaee2c7c9abceb15f9d4',1),
(57,'mollerwa','dmoll@umich.edu','$2y$10$0izpUXcLR6Gz5UzkIW6ULeNlC76n2Ws2MZZVqzBXU3lB//hid6kV.','2017-06-18 16:32:19',4,1,'3aa60f4d4e23fb5f2c48a68fdbb955405b504aba',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_premium_avatar`
--

DROP TABLE IF EXISTS `user_premium_avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_premium_avatar` (
  `user_id` int(11) NOT NULL,
  `avatar_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`avatar_id`),
  KEY `fk_user_has_avatar_avatar1_idx` (`avatar_id`),
  KEY `fk_user_has_avatar_user_idx` (`user_id`),
  CONSTRAINT `fk_user_has_avatar_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_avatar_avatar1` FOREIGN KEY (`avatar_id`) REFERENCES `avatar` (`avatar_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_premium_avatar`
--

LOCK TABLES `user_premium_avatar` WRITE;
/*!40000 ALTER TABLE `user_premium_avatar` DISABLE KEYS */;
INSERT INTO `user_premium_avatar` VALUES (1,8,NULL),(1,9,NULL),(2,8,NULL),(2,10,NULL),(2,11,NULL);
/*!40000 ALTER TABLE `user_premium_avatar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-21 10:24:04
