$("document").ready(function() {
    $("#avatar_change").click(function() {
        $("#dialog").dialog("open");
    });
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "fade",
                duration: 600
            },
            hide: {
                effect: "fade",
                duration: 600
            }
        });
    });

});

function changeavatar(id) {
    window.location.replace("profile/update/" + id);
}