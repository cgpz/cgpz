$(document).ready(function() {
    //Send Ready if not expired
    play('nU', '');
    sv();
});

$(document).keypress( function(event) {
    if(event.which == 32) {//space pressed
        event.preventDefault();
        play('rU');
    }
});
var canvas = document.getElementById("canvas");;
var context = canvas.getContext("2d");
var ready = false;
var ingame = false;
var imgs = {};
var readyState = {};
var gameData;
var len = 0, w = 0, margin = canvas.width/100 * 7;
/*
window.requestAnimFrame = window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function(callback, element){
            window.setTimeout(callback, 1000 / 60);
        };
*/
function play(req, mu){
    $.post("game/get" , {
        req: req,
        user: $('#game-data').data('user'),
        id: $('#game-data').data('sid'),
        av: $('#game-data').data('av'),
        gameSessionId: $('#game-data').data('session'),
        mu: mu
    }, function (data, status) {
        gameData = JSON.parse(data);
        //gameData = data;
    });
}

function sv(){
    setInterval ( function () {
        //get state of game
        play('cc', '');
        if(!gameData) {
            //if session has expired
            window.location.replace('lobby');
        } else {
            //go through users, set their imgs and check last exec
            for(var idx in gameData['state']['users']){
                if(! imgs[idx]) {
                    imgs[idx] = new Image();
                    imgs[idx].src = "../assets/img/" + gameData['state']['users'][idx]['av'];
                }
                //check if ready if not ingame phase
                if(!ingame) {

                }
                //console.log(imgs[idx]);
            };
        }
        draw();

    }, 200);
}

function draw() {
    if(!gameData) {
        window.location.replace('lobby');
    } else {
        //canvas
        canvas.width = canvas.width;
        //background
        var grd = context.createRadialGradient(
            canvas.width/2+margin,
            canvas.width/2+margin,
            50,
            canvas.width/2 ,
            canvas.width/2,
            400);
        grd.addColorStop(0,"#cdd");
        grd.addColorStop(1,"#bdE");
        context.fillStyle = grd;
        context.fillRect(0,0,canvas.width,canvas.width);

        //text for users
        context.shadowColor = "black";
        context.shadowOffsetX = 2;
        context.shadowOffsetY = 2;
        context.shadowBlur = 2;
        context.scale(1,1);
        //text
        len = imgs.length;
        var w = canvas.width / 4 - margin;
        for(var idx in gameData['state']['users']) {
            if (imgs[idx]) {
                if (idx == gameData['state']['owner']){
                    context.drawImage(imgs[idx], w * idx - 50, canvas.height - 150, 130, 130);
                    setFillStyle(idx);
                    context.fillText(gameData['state']['users'][idx]['user'], w * idx - 50, canvas.height - 170)
                } else {
                    context.drawImage(imgs[idx], w * idx, canvas.height - 100, 75, 75);
                    setFillStyle(idx);
                    context.fillText(gameData['state']['users'][idx]['user'], w * idx, canvas.height - 110);
                }
            }
        }

        var text = '';
        //draw main tooltip
        if(!ready) {
            text = 'Press Space To Start';
            setFillStyle(0);
            context.fillText(text, 250, 150);
        }
        //context.fillText(text, 50, 50);
        var alpha = alpha - 0.05; // decrease opacity (fade out)
        if (alpha < 0) {
            text = '';
            //canvas.width = canvas.width;
            //clearInterval(interval);
        }
    }
}
/*
frame();
function frame () {
    requestAnimFrame( frame );
    draw();
}
*/


function setFillStyle(idx){

    if(idx == $('#game-data').data("sid")){//if current user
        context.font="30px Verdana";
        var gradient=context.createLinearGradient(0,0,600,0);
        gradient.addColorStop("0","cyan");
        gradient.addColorStop("1.0","blue");
        context.fillStyle=gradient;
    } else if (idx == gameData['state']['owner']) {
        context.font = "italic 16pt Verdana";
        context.fillStyle = "rgba(78, 180, 210)";
    } else {
        context.font = "italic 16pt Verdana";
        context.fillStyle = "rgba(0, 0, 0)";
    }
}