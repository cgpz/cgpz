$("document").ready(function() {
    $("#newgame").click(function() {
        $("#create-dialog").dialog("open");
    });
    $( function() {
        $( "#create-dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "fade",
                duration: 600
            },
            hide: {
                effect: "fade",
                duration: 600
            }
        });
    });

});